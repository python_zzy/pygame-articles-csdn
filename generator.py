obj = "pymunk"
res = ["# Python {}最完整教程 总目录".format(obj), "专栏地址: {}"]

with open(f"{obj}/trees.py", encoding="utf8") as f:
    exec(f.read().rstrip("\n"))

for count, (link, tree) in enumerate(trees, start=1):
    assert "下一篇文章" not in tree

    res.append(f"## ({count})")
    res.append(f"link: <{link}>")

    for i, title in enumerate(tree.split("\n")):
        if title == "": continue
        res.append(f"[{title}]({link}#t{i})")

with open(f"{obj}/README.md", "w", encoding="utf8") as f:
    f.write("\n\n".join(res))

