trees = (("https://blog.csdn.net/qq_48979387/article/details/125706562","""1 走进tkinter世界
1.1 认识tkinter
1.2 tkinter的坐标系与颜色格式
坐标系
颜色
1.3 创建根窗口
1.4 组件
2 tkinter主模块
2.1 Label
创建Label
relief参数
cursor参数
font参数
bitmap参数
image参数
textvariable参数
2.2 Button
创建Button
activeforeground和activebackground参数
repeatdelay和repeatinterval参数
禁用按钮
2.3 布局管理(pack,grid,place)
pack
grid
place
更改组件映射
布局管理
2.4 Frame
创建Frame
Frame的作用
2.5 LabelFrame
创建LabelFrame
labelanchor参数
labelwidget参数
2.6 Entry
创建Entry
show参数
get方法
insert和delete方法
readonly状态
输入验证
实例：密码验证系统
2.7 事件绑定
按键名称
sequence
<Type-Detail>
Event
<Modifier-Type-Detail>
bind_all方法
虚拟事件
bindtags方法
unbind方法
关于事件绑定的实例
2.8 Variable(tkinter变量)
trace_add方法
trace传递给回调函数的参数
trace_remove方法
trace_info方法
2.9 Toplevel
创建Toplevel"""),
("https://blog.csdn.net/qq_48979387/article/details/125806479","""2 tkinter主模块
2.10 Checkbutton
创建Checkbutton
variable参数
indicatoron参数
2.11 Radiobutton
创建Radiobutton
2.12 Menu
创建窗口菜单
add方法
add_command方法
add_cascade方法
分离菜单
accelerator参数
underline参数
add_separator方法
弹出菜单
2.13 Menubutton
创建Menubutton
2.14 组件基类
2.15 Listbox
创建Listbox
虚拟事件<<ListboxSelect>>
see方法
实例：列表编辑器
2.16 Scrollbar
创建Scrollbar
XView和YView类
orient参数
滚动条绑定组件
2.17 Message
2.18 Scale
创建Scale
from_和to参数
tickinterval参数
label参数
resolution参数
实例：颜色调节器
2.19 Spinbox
创建Spinbox
from_, to和increment参数
values参数
readonly状态
2.20 OptionMenu
创建OptionMenu
2.21 PanedWindow
创建PanedWindow
add方法
showhandle参数
2.22 Text
wrap参数
创建Text
撤销和重做
索引
插入文字
插入图片
插入组件
Mark（标记）
Tag（标签）
get和dump方法
search方法
2.23 tkinter图片对象
PhotoImage
BitmapImage"""),
("https://blog.csdn.net/qq_48979387/article/details/125768994","""2 tkinter主模块
2.24 Canvas
create_arc方法
create_bitmap方法
create_image方法
create_line方法
create_oval方法
create_polygon方法
create_rectangle方法
create_text方法
create_window方法
表示画布对象
postscript方法
实例：接球游戏
3 tkinter子模块
3.1 tkinter.constants
3.2 tkinter.messagebox
实例：询问是否继续
3.3 tkinter.filedialog 
实例：文本查看器
3.4 tkinter.colorchooser
3.5 tkinter.ttk
组件功能的区别
组件风格的区别
组件状态
3.6 ttk.Style
configure方法
map方法
Element（元素）
layout方法
Theme（主题）
3.7 ttk.Combobox
创建Combobox
虚拟事件<<ComboboxSelected>>
3.8 ttk.Separator
创建Separator
3.9 ttk.Sizegrip
创建Sizegrip
3.10 ttk.Notebook
创建Notebook
add方法
索引
虚拟事件<<NotebookTabChanged>>
3.11 ttk.Progressbar
创建Progressbar
start和stop方法
mode参数
3.12 ttk.Treeview
创建Treeview
heading方法
insert方法
虚拟事件
3.13 tkinter.font
Font类
font模块的其他方法
载入目录下的字体文件
3.14 tkinter.scrolledtext
3.15 更多子模块
附录1 关于tkinter的网站
附录2 调用Tcl/Tk命令
附录3 颜色列表"""))
