# Python tkinter最完整教程 总目录

专栏地址: https://blog.csdn.net/qq_48979387/category_11290849.html

## (1)

link: <https://blog.csdn.net/qq_48979387/article/details/125706562>

[1 走进tkinter世界](https://blog.csdn.net/qq_48979387/article/details/125706562#t0)

[1.1 认识tkinter](https://blog.csdn.net/qq_48979387/article/details/125706562#t1)

[1.2 tkinter的坐标系与颜色格式](https://blog.csdn.net/qq_48979387/article/details/125706562#t2)

[坐标系](https://blog.csdn.net/qq_48979387/article/details/125706562#t3)

[颜色](https://blog.csdn.net/qq_48979387/article/details/125706562#t4)

[1.3 创建根窗口](https://blog.csdn.net/qq_48979387/article/details/125706562#t5)

[1.4 组件](https://blog.csdn.net/qq_48979387/article/details/125706562#t6)

[2 tkinter主模块](https://blog.csdn.net/qq_48979387/article/details/125706562#t7)

[2.1 Label](https://blog.csdn.net/qq_48979387/article/details/125706562#t8)

[创建Label](https://blog.csdn.net/qq_48979387/article/details/125706562#t9)

[relief参数](https://blog.csdn.net/qq_48979387/article/details/125706562#t10)

[cursor参数](https://blog.csdn.net/qq_48979387/article/details/125706562#t11)

[font参数](https://blog.csdn.net/qq_48979387/article/details/125706562#t12)

[bitmap参数](https://blog.csdn.net/qq_48979387/article/details/125706562#t13)

[image参数](https://blog.csdn.net/qq_48979387/article/details/125706562#t14)

[textvariable参数](https://blog.csdn.net/qq_48979387/article/details/125706562#t15)

[2.2 Button](https://blog.csdn.net/qq_48979387/article/details/125706562#t16)

[创建Button](https://blog.csdn.net/qq_48979387/article/details/125706562#t17)

[activeforeground和activebackground参数](https://blog.csdn.net/qq_48979387/article/details/125706562#t18)

[repeatdelay和repeatinterval参数](https://blog.csdn.net/qq_48979387/article/details/125706562#t19)

[禁用按钮](https://blog.csdn.net/qq_48979387/article/details/125706562#t20)

[2.3 布局管理(pack,grid,place)](https://blog.csdn.net/qq_48979387/article/details/125706562#t21)

[pack](https://blog.csdn.net/qq_48979387/article/details/125706562#t22)

[grid](https://blog.csdn.net/qq_48979387/article/details/125706562#t23)

[place](https://blog.csdn.net/qq_48979387/article/details/125706562#t24)

[更改组件映射](https://blog.csdn.net/qq_48979387/article/details/125706562#t25)

[布局管理](https://blog.csdn.net/qq_48979387/article/details/125706562#t26)

[2.4 Frame](https://blog.csdn.net/qq_48979387/article/details/125706562#t27)

[创建Frame](https://blog.csdn.net/qq_48979387/article/details/125706562#t28)

[Frame的作用](https://blog.csdn.net/qq_48979387/article/details/125706562#t29)

[2.5 LabelFrame](https://blog.csdn.net/qq_48979387/article/details/125706562#t30)

[创建LabelFrame](https://blog.csdn.net/qq_48979387/article/details/125706562#t31)

[labelanchor参数](https://blog.csdn.net/qq_48979387/article/details/125706562#t32)

[labelwidget参数](https://blog.csdn.net/qq_48979387/article/details/125706562#t33)

[2.6 Entry](https://blog.csdn.net/qq_48979387/article/details/125706562#t34)

[创建Entry](https://blog.csdn.net/qq_48979387/article/details/125706562#t35)

[show参数](https://blog.csdn.net/qq_48979387/article/details/125706562#t36)

[get方法](https://blog.csdn.net/qq_48979387/article/details/125706562#t37)

[insert和delete方法](https://blog.csdn.net/qq_48979387/article/details/125706562#t38)

[readonly状态](https://blog.csdn.net/qq_48979387/article/details/125706562#t39)

[输入验证](https://blog.csdn.net/qq_48979387/article/details/125706562#t40)

[实例：密码验证系统](https://blog.csdn.net/qq_48979387/article/details/125706562#t41)

[2.7 事件绑定](https://blog.csdn.net/qq_48979387/article/details/125706562#t42)

[按键名称](https://blog.csdn.net/qq_48979387/article/details/125706562#t43)

[sequence](https://blog.csdn.net/qq_48979387/article/details/125706562#t44)

[<Type-Detail>](https://blog.csdn.net/qq_48979387/article/details/125706562#t45)

[Event](https://blog.csdn.net/qq_48979387/article/details/125706562#t46)

[<Modifier-Type-Detail>](https://blog.csdn.net/qq_48979387/article/details/125706562#t47)

[bind_all方法](https://blog.csdn.net/qq_48979387/article/details/125706562#t48)

[虚拟事件](https://blog.csdn.net/qq_48979387/article/details/125706562#t49)

[bindtags方法](https://blog.csdn.net/qq_48979387/article/details/125706562#t50)

[unbind方法](https://blog.csdn.net/qq_48979387/article/details/125706562#t51)

[关于事件绑定的实例](https://blog.csdn.net/qq_48979387/article/details/125706562#t52)

[2.8 Variable(tkinter变量)](https://blog.csdn.net/qq_48979387/article/details/125706562#t53)

[trace_add方法](https://blog.csdn.net/qq_48979387/article/details/125706562#t54)

[trace传递给回调函数的参数](https://blog.csdn.net/qq_48979387/article/details/125706562#t55)

[trace_remove方法](https://blog.csdn.net/qq_48979387/article/details/125706562#t56)

[trace_info方法](https://blog.csdn.net/qq_48979387/article/details/125706562#t57)

[2.9 Toplevel](https://blog.csdn.net/qq_48979387/article/details/125706562#t58)

[创建Toplevel](https://blog.csdn.net/qq_48979387/article/details/125706562#t59)

## (2)

link: <https://blog.csdn.net/qq_48979387/article/details/125806479>

[2 tkinter主模块](https://blog.csdn.net/qq_48979387/article/details/125806479#t0)

[2.10 Checkbutton](https://blog.csdn.net/qq_48979387/article/details/125806479#t1)

[创建Checkbutton](https://blog.csdn.net/qq_48979387/article/details/125806479#t2)

[variable参数](https://blog.csdn.net/qq_48979387/article/details/125806479#t3)

[indicatoron参数](https://blog.csdn.net/qq_48979387/article/details/125806479#t4)

[2.11 Radiobutton](https://blog.csdn.net/qq_48979387/article/details/125806479#t5)

[创建Radiobutton](https://blog.csdn.net/qq_48979387/article/details/125806479#t6)

[2.12 Menu](https://blog.csdn.net/qq_48979387/article/details/125806479#t7)

[创建窗口菜单](https://blog.csdn.net/qq_48979387/article/details/125806479#t8)

[add方法](https://blog.csdn.net/qq_48979387/article/details/125806479#t9)

[add_command方法](https://blog.csdn.net/qq_48979387/article/details/125806479#t10)

[add_cascade方法](https://blog.csdn.net/qq_48979387/article/details/125806479#t11)

[分离菜单](https://blog.csdn.net/qq_48979387/article/details/125806479#t12)

[accelerator参数](https://blog.csdn.net/qq_48979387/article/details/125806479#t13)

[underline参数](https://blog.csdn.net/qq_48979387/article/details/125806479#t14)

[add_separator方法](https://blog.csdn.net/qq_48979387/article/details/125806479#t15)

[弹出菜单](https://blog.csdn.net/qq_48979387/article/details/125806479#t16)

[2.13 Menubutton](https://blog.csdn.net/qq_48979387/article/details/125806479#t17)

[创建Menubutton](https://blog.csdn.net/qq_48979387/article/details/125806479#t18)

[2.14 组件基类](https://blog.csdn.net/qq_48979387/article/details/125806479#t19)

[2.15 Listbox](https://blog.csdn.net/qq_48979387/article/details/125806479#t20)

[创建Listbox](https://blog.csdn.net/qq_48979387/article/details/125806479#t21)

[虚拟事件<<ListboxSelect>>](https://blog.csdn.net/qq_48979387/article/details/125806479#t22)

[see方法](https://blog.csdn.net/qq_48979387/article/details/125806479#t23)

[实例：列表编辑器](https://blog.csdn.net/qq_48979387/article/details/125806479#t24)

[2.16 Scrollbar](https://blog.csdn.net/qq_48979387/article/details/125806479#t25)

[创建Scrollbar](https://blog.csdn.net/qq_48979387/article/details/125806479#t26)

[XView和YView类](https://blog.csdn.net/qq_48979387/article/details/125806479#t27)

[orient参数](https://blog.csdn.net/qq_48979387/article/details/125806479#t28)

[滚动条绑定组件](https://blog.csdn.net/qq_48979387/article/details/125806479#t29)

[2.17 Message](https://blog.csdn.net/qq_48979387/article/details/125806479#t30)

[2.18 Scale](https://blog.csdn.net/qq_48979387/article/details/125806479#t31)

[创建Scale](https://blog.csdn.net/qq_48979387/article/details/125806479#t32)

[from_和to参数](https://blog.csdn.net/qq_48979387/article/details/125806479#t33)

[tickinterval参数](https://blog.csdn.net/qq_48979387/article/details/125806479#t34)

[label参数](https://blog.csdn.net/qq_48979387/article/details/125806479#t35)

[resolution参数](https://blog.csdn.net/qq_48979387/article/details/125806479#t36)

[实例：颜色调节器](https://blog.csdn.net/qq_48979387/article/details/125806479#t37)

[2.19 Spinbox](https://blog.csdn.net/qq_48979387/article/details/125806479#t38)

[创建Spinbox](https://blog.csdn.net/qq_48979387/article/details/125806479#t39)

[from_, to和increment参数](https://blog.csdn.net/qq_48979387/article/details/125806479#t40)

[values参数](https://blog.csdn.net/qq_48979387/article/details/125806479#t41)

[readonly状态](https://blog.csdn.net/qq_48979387/article/details/125806479#t42)

[2.20 OptionMenu](https://blog.csdn.net/qq_48979387/article/details/125806479#t43)

[创建OptionMenu](https://blog.csdn.net/qq_48979387/article/details/125806479#t44)

[2.21 PanedWindow](https://blog.csdn.net/qq_48979387/article/details/125806479#t45)

[创建PanedWindow](https://blog.csdn.net/qq_48979387/article/details/125806479#t46)

[add方法](https://blog.csdn.net/qq_48979387/article/details/125806479#t47)

[showhandle参数](https://blog.csdn.net/qq_48979387/article/details/125806479#t48)

[2.22 Text](https://blog.csdn.net/qq_48979387/article/details/125806479#t49)

[wrap参数](https://blog.csdn.net/qq_48979387/article/details/125806479#t50)

[创建Text](https://blog.csdn.net/qq_48979387/article/details/125806479#t51)

[撤销和重做](https://blog.csdn.net/qq_48979387/article/details/125806479#t52)

[索引](https://blog.csdn.net/qq_48979387/article/details/125806479#t53)

[插入文字](https://blog.csdn.net/qq_48979387/article/details/125806479#t54)

[插入图片](https://blog.csdn.net/qq_48979387/article/details/125806479#t55)

[插入组件](https://blog.csdn.net/qq_48979387/article/details/125806479#t56)

[Mark（标记）](https://blog.csdn.net/qq_48979387/article/details/125806479#t57)

[Tag（标签）](https://blog.csdn.net/qq_48979387/article/details/125806479#t58)

[get和dump方法](https://blog.csdn.net/qq_48979387/article/details/125806479#t59)

[search方法](https://blog.csdn.net/qq_48979387/article/details/125806479#t60)

[2.23 tkinter图片对象](https://blog.csdn.net/qq_48979387/article/details/125806479#t61)

[PhotoImage](https://blog.csdn.net/qq_48979387/article/details/125806479#t62)

[BitmapImage](https://blog.csdn.net/qq_48979387/article/details/125806479#t63)

## (3)

link: <https://blog.csdn.net/qq_48979387/article/details/125768994>

[2 tkinter主模块](https://blog.csdn.net/qq_48979387/article/details/125768994#t0)

[2.24 Canvas](https://blog.csdn.net/qq_48979387/article/details/125768994#t1)

[create_arc方法](https://blog.csdn.net/qq_48979387/article/details/125768994#t2)

[create_bitmap方法](https://blog.csdn.net/qq_48979387/article/details/125768994#t3)

[create_image方法](https://blog.csdn.net/qq_48979387/article/details/125768994#t4)

[create_line方法](https://blog.csdn.net/qq_48979387/article/details/125768994#t5)

[create_oval方法](https://blog.csdn.net/qq_48979387/article/details/125768994#t6)

[create_polygon方法](https://blog.csdn.net/qq_48979387/article/details/125768994#t7)

[create_rectangle方法](https://blog.csdn.net/qq_48979387/article/details/125768994#t8)

[create_text方法](https://blog.csdn.net/qq_48979387/article/details/125768994#t9)

[create_window方法](https://blog.csdn.net/qq_48979387/article/details/125768994#t10)

[表示画布对象](https://blog.csdn.net/qq_48979387/article/details/125768994#t11)

[postscript方法](https://blog.csdn.net/qq_48979387/article/details/125768994#t12)

[实例：接球游戏](https://blog.csdn.net/qq_48979387/article/details/125768994#t13)

[3 tkinter子模块](https://blog.csdn.net/qq_48979387/article/details/125768994#t14)

[3.1 tkinter.constants](https://blog.csdn.net/qq_48979387/article/details/125768994#t15)

[3.2 tkinter.messagebox](https://blog.csdn.net/qq_48979387/article/details/125768994#t16)

[实例：询问是否继续](https://blog.csdn.net/qq_48979387/article/details/125768994#t17)

[3.3 tkinter.filedialog ](https://blog.csdn.net/qq_48979387/article/details/125768994#t18)

[实例：文本查看器](https://blog.csdn.net/qq_48979387/article/details/125768994#t19)

[3.4 tkinter.colorchooser](https://blog.csdn.net/qq_48979387/article/details/125768994#t20)

[3.5 tkinter.ttk](https://blog.csdn.net/qq_48979387/article/details/125768994#t21)

[组件功能的区别](https://blog.csdn.net/qq_48979387/article/details/125768994#t22)

[组件风格的区别](https://blog.csdn.net/qq_48979387/article/details/125768994#t23)

[组件状态](https://blog.csdn.net/qq_48979387/article/details/125768994#t24)

[3.6 ttk.Style](https://blog.csdn.net/qq_48979387/article/details/125768994#t25)

[configure方法](https://blog.csdn.net/qq_48979387/article/details/125768994#t26)

[map方法](https://blog.csdn.net/qq_48979387/article/details/125768994#t27)

[Element（元素）](https://blog.csdn.net/qq_48979387/article/details/125768994#t28)

[layout方法](https://blog.csdn.net/qq_48979387/article/details/125768994#t29)

[Theme（主题）](https://blog.csdn.net/qq_48979387/article/details/125768994#t30)

[3.7 ttk.Combobox](https://blog.csdn.net/qq_48979387/article/details/125768994#t31)

[创建Combobox](https://blog.csdn.net/qq_48979387/article/details/125768994#t32)

[虚拟事件<<ComboboxSelected>>](https://blog.csdn.net/qq_48979387/article/details/125768994#t33)

[3.8 ttk.Separator](https://blog.csdn.net/qq_48979387/article/details/125768994#t34)

[创建Separator](https://blog.csdn.net/qq_48979387/article/details/125768994#t35)

[3.9 ttk.Sizegrip](https://blog.csdn.net/qq_48979387/article/details/125768994#t36)

[创建Sizegrip](https://blog.csdn.net/qq_48979387/article/details/125768994#t37)

[3.10 ttk.Notebook](https://blog.csdn.net/qq_48979387/article/details/125768994#t38)

[创建Notebook](https://blog.csdn.net/qq_48979387/article/details/125768994#t39)

[add方法](https://blog.csdn.net/qq_48979387/article/details/125768994#t40)

[索引](https://blog.csdn.net/qq_48979387/article/details/125768994#t41)

[虚拟事件<<NotebookTabChanged>>](https://blog.csdn.net/qq_48979387/article/details/125768994#t42)

[3.11 ttk.Progressbar](https://blog.csdn.net/qq_48979387/article/details/125768994#t43)

[创建Progressbar](https://blog.csdn.net/qq_48979387/article/details/125768994#t44)

[start和stop方法](https://blog.csdn.net/qq_48979387/article/details/125768994#t45)

[mode参数](https://blog.csdn.net/qq_48979387/article/details/125768994#t46)

[3.12 ttk.Treeview](https://blog.csdn.net/qq_48979387/article/details/125768994#t47)

[创建Treeview](https://blog.csdn.net/qq_48979387/article/details/125768994#t48)

[heading方法](https://blog.csdn.net/qq_48979387/article/details/125768994#t49)

[insert方法](https://blog.csdn.net/qq_48979387/article/details/125768994#t50)

[虚拟事件](https://blog.csdn.net/qq_48979387/article/details/125768994#t51)

[3.13 tkinter.font](https://blog.csdn.net/qq_48979387/article/details/125768994#t52)

[Font类](https://blog.csdn.net/qq_48979387/article/details/125768994#t53)

[font模块的其他方法](https://blog.csdn.net/qq_48979387/article/details/125768994#t54)

[载入目录下的字体文件](https://blog.csdn.net/qq_48979387/article/details/125768994#t55)

[3.14 tkinter.scrolledtext](https://blog.csdn.net/qq_48979387/article/details/125768994#t56)

[3.15 更多子模块](https://blog.csdn.net/qq_48979387/article/details/125768994#t57)

[附录1 关于tkinter的网站](https://blog.csdn.net/qq_48979387/article/details/125768994#t58)

[附录2 调用Tcl/Tk命令](https://blog.csdn.net/qq_48979387/article/details/125768994#t59)

[附录3 颜色列表](https://blog.csdn.net/qq_48979387/article/details/125768994#t60)