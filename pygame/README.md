# Python pygame最完整教程 总目录

专栏地址: <https://blog.csdn.net/qq_48979387/category_12189169.html>

## (1)

link: <https://blog.csdn.net/qq_48979387/article/details/126799308>

[1 初识pygame](https://blog.csdn.net/qq_48979387/article/details/126799308#t0)

[1.1 简介](https://blog.csdn.net/qq_48979387/article/details/126799308#t1)

[1.2 pygame的优势](https://blog.csdn.net/qq_48979387/article/details/126799308#t2)

[1.3 安装pygame](https://blog.csdn.net/qq_48979387/article/details/126799308#t3)

[1.4 pygame子模块](https://blog.csdn.net/qq_48979387/article/details/126799308#t4)

[1.5 关于本教程](https://blog.csdn.net/qq_48979387/article/details/126799308#t5)

[1.6 表面](https://blog.csdn.net/qq_48979387/article/details/126799308#t6)

[2 第一个pygame示例](https://blog.csdn.net/qq_48979387/article/details/126799308#t7)

[2.1 初始化pygame](https://blog.csdn.net/qq_48979387/article/details/126799308#t8)

[2.2 创建窗口](https://blog.csdn.net/qq_48979387/article/details/126799308#t9)

[2.3 更改标题](https://blog.csdn.net/qq_48979387/article/details/126799308#t10)

[2.4 事件循环](https://blog.csdn.net/qq_48979387/article/details/126799308#t11)

[2.5 退出pygame](https://blog.csdn.net/qq_48979387/article/details/126799308#t12)

[3 基础表面操作](https://blog.csdn.net/qq_48979387/article/details/126799308#t13)

[3.1 fill()方法](https://blog.csdn.net/qq_48979387/article/details/126799308#t14)

[3.2 载入图片](https://blog.csdn.net/qq_48979387/article/details/126799308#t15)

[3.3 blit()方法](https://blog.csdn.net/qq_48979387/article/details/126799308#t16)

[3.4 Rect对象](https://blog.csdn.net/qq_48979387/article/details/126799308#t17)

[3.5 实现动画](https://blog.csdn.net/qq_48979387/article/details/126799308#t18)

[4 事件控制](https://blog.csdn.net/qq_48979387/article/details/126799308#t19)

[4.1 事件类型](https://blog.csdn.net/qq_48979387/article/details/126799308#t20)

[4.2 键盘事件](https://blog.csdn.net/qq_48979387/article/details/126799308#t21)

[4.3 鼠标事件](https://blog.csdn.net/qq_48979387/article/details/126799308#t22)

[4.4 窗口焦点事件](https://blog.csdn.net/qq_48979387/article/details/126799308#t23)

[4.5 拖拽文件或文本事件](https://blog.csdn.net/qq_48979387/article/details/126799308#t24)

[4.6 文本输入事件](https://blog.csdn.net/qq_48979387/article/details/126799308#t25)

[4.7 发送自定义事件](https://blog.csdn.net/qq_48979387/article/details/126799308#t26)

[4.8 阻塞事件](https://blog.csdn.net/qq_48979387/article/details/126799308#t27)

[4.9 event模块索引-事件处理](https://blog.csdn.net/qq_48979387/article/details/126799308#t28)

[5 时间控制](https://blog.csdn.net/qq_48979387/article/details/126799308#t29)

[5.1 控制FPS](https://blog.csdn.net/qq_48979387/article/details/126799308#t30)

[5.2 获取游戏运行时间](https://blog.csdn.net/qq_48979387/article/details/126799308#t31)

[5.3 定期发送事件](https://blog.csdn.net/qq_48979387/article/details/126799308#t32)

[5.4 time模块索引-时间控制](https://blog.csdn.net/qq_48979387/article/details/126799308#t33)

[实战 行走的人](https://blog.csdn.net/qq_48979387/article/details/126799308#t34)

[完整代码](https://blog.csdn.net/qq_48979387/article/details/126799308#t35)

[准备素材](https://blog.csdn.net/qq_48979387/article/details/126799308#t36)

[pygame.locals](https://blog.csdn.net/qq_48979387/article/details/126799308#t37)

[导入模块、定义常量](https://blog.csdn.net/qq_48979387/article/details/126799308#t38)

[加载帧序列](https://blog.csdn.net/qq_48979387/article/details/126799308#t39)

[保证玩家在屏幕内移动](https://blog.csdn.net/qq_48979387/article/details/126799308#t40)

## (2)

link: <https://blog.csdn.net/qq_48979387/article/details/128784116>

[6 文字绘制](https://blog.csdn.net/qq_48979387/article/details/128784116#t0)

[6.1 载入字体](https://blog.csdn.net/qq_48979387/article/details/128784116#t1)

[6.2 渲染字体](https://blog.csdn.net/qq_48979387/article/details/128784116#t2)

[6.3 字体特殊样式](https://blog.csdn.net/qq_48979387/article/details/128784116#t3)

[6.4 文本自动换行](https://blog.csdn.net/qq_48979387/article/details/128784116#t4)

[6.5 文本绘制方向](https://blog.csdn.net/qq_48979387/article/details/128784116#t5)

[6.6 font模块索引-字体操作](https://blog.csdn.net/qq_48979387/article/details/128784116#t6)

[6.7 freetype模块索引-字体操作扩展](https://blog.csdn.net/qq_48979387/article/details/128784116#t7)

[7 按键处理](https://blog.csdn.net/qq_48979387/article/details/128784116#t8)

[7.1 获取持续按下的按键](https://blog.csdn.net/qq_48979387/article/details/128784116#t9)

[7.2 获取组合键](https://blog.csdn.net/qq_48979387/article/details/128784116#t10)

[7.3 控制重复触发KEYDOWN事件](https://blog.csdn.net/qq_48979387/article/details/128784116#t11)

[7.4 更改文本输入候选框位置](https://blog.csdn.net/qq_48979387/article/details/128784116#t12)

[7.5 key模块索引-按键操作](https://blog.csdn.net/qq_48979387/article/details/128784116#t13)

[8 鼠标处理](https://blog.csdn.net/qq_48979387/article/details/128784116#t14)

[8.1 获取鼠标位置](https://blog.csdn.net/qq_48979387/article/details/128784116#t15)

[8.2 隐藏和显示光标](https://blog.csdn.net/qq_48979387/article/details/128784116#t16)

[8.3 光标样式](https://blog.csdn.net/qq_48979387/article/details/128784116#t17)

[8.4 mouse模块索引-鼠标操作](https://blog.csdn.net/qq_48979387/article/details/128784116#t18)

[8.5 cursor模块索引-光标样式](https://blog.csdn.net/qq_48979387/article/details/128784116#t19)

[实战：键盘输入程序](https://blog.csdn.net/qq_48979387/article/details/128784116#t20)

[完整代码](https://blog.csdn.net/qq_48979387/article/details/128784116#t21)

## (3)

link: <https://blog.csdn.net/qq_48979387/article/details/128865416>

[9 颜色](https://blog.csdn.net/qq_48979387/article/details/128865416#t0)

[9.1 关于计算机中的颜色](https://blog.csdn.net/qq_48979387/article/details/128865416#t1)

[9.2 Color对象](https://blog.csdn.net/qq_48979387/article/details/128865416#t2)

[9.3 颜色列表](https://blog.csdn.net/qq_48979387/article/details/128865416#t3)

[10 进阶表面操作](https://blog.csdn.net/qq_48979387/article/details/128865416#t4)

[10.1 Surface](https://blog.csdn.net/qq_48979387/article/details/128865416#t5)

[10.2 表面格式转换](https://blog.csdn.net/qq_48979387/article/details/128865416#t6)

[10.3 表面透明度](https://blog.csdn.net/qq_48979387/article/details/128865416#t7)

[10.4 颜色键](https://blog.csdn.net/qq_48979387/article/details/128865416#t8)

[10.5 绘制表面](https://blog.csdn.net/qq_48979387/article/details/128865416#t9)

[10.6 剪裁矩形](https://blog.csdn.net/qq_48979387/article/details/128865416#t10)

[10.7 子表面](https://blog.csdn.net/qq_48979387/article/details/128865416#t11)

[10.8 调色板](https://blog.csdn.net/qq_48979387/article/details/128865416#t12)

[10.9 Surface对象方法索引](https://blog.csdn.net/qq_48979387/article/details/128865416#t13)

[10.10 图像载入与保存](https://blog.csdn.net/qq_48979387/article/details/128865416#t14)

[10.11 原始码和表面的转换](https://blog.csdn.net/qq_48979387/article/details/128865416#t15)

[10.12 image模块索引-图像操作](https://blog.csdn.net/qq_48979387/article/details/128865416#t16)

[10.13 缩放表面](https://blog.csdn.net/qq_48979387/article/details/128865416#t17)

[10.14 旋转表面](https://blog.csdn.net/qq_48979387/article/details/128865416#t18)

[10.15 表面阈值处理](https://blog.csdn.net/qq_48979387/article/details/128865416#t19)

[10.16 transform模块索引-变换表面](https://blog.csdn.net/qq_48979387/article/details/128865416#t20)

[10.17 mask模块索引-掩码](https://blog.csdn.net/qq_48979387/article/details/128865416#t21)

## (4)

link: <https://blog.csdn.net/qq_48979387/article/details/128994501>

[11 图形](https://blog.csdn.net/qq_48979387/article/details/128994501#t0)

[11.1 绘制矩形或圆角矩形](https://blog.csdn.net/qq_48979387/article/details/128994501#t1)

[11.2 绘制圆形或半圆形](https://blog.csdn.net/qq_48979387/article/details/128994501#t2)

[11.3 绘制多边形](https://blog.csdn.net/qq_48979387/article/details/128994501#t3)

[11.4 draw模块索引-图形绘制](https://blog.csdn.net/qq_48979387/article/details/128994501#t4)

[12 精灵与碰撞检测](https://blog.csdn.net/qq_48979387/article/details/128994501#t5)

[12.1 精灵、精灵组](https://blog.csdn.net/qq_48979387/article/details/128994501#t6)

[12.2 精灵、精灵组的碰撞检测](https://blog.csdn.net/qq_48979387/article/details/128994501#t7)

[12.3 sprite模块索引-管理精灵对象](https://blog.csdn.net/qq_48979387/article/details/128994501#t8)

[12.4 矩形与矩形的碰撞检测](https://blog.csdn.net/qq_48979387/article/details/128994501#t9)

[12.5 矩形与点的碰撞检测](https://blog.csdn.net/qq_48979387/article/details/128994501#t10)

[12.6 矩形与线段的碰撞检测](https://blog.csdn.net/qq_48979387/article/details/128994501#t11)

[12.7 完美碰撞检测](https://blog.csdn.net/qq_48979387/article/details/128994501#t12)

[实战：飞机大战游戏](https://blog.csdn.net/qq_48979387/article/details/128994501#t13)

[完整代码](https://blog.csdn.net/qq_48979387/article/details/128994501#t14)

[缓存算法](https://blog.csdn.net/qq_48979387/article/details/128994501#t15)

[分割单张帧序列图片](https://blog.csdn.net/qq_48979387/article/details/128994501#t16)

## (5)

link: <https://blog.csdn.net/qq_48979387/article/details/129219749>

[13 显示](https://blog.csdn.net/qq_48979387/article/details/129219749#t0)

[13.1 创建窗口](https://blog.csdn.net/qq_48979387/article/details/129219749#t1)

[13.2 设置标题和图标](https://blog.csdn.net/qq_48979387/article/details/129219749#t2)

[13.3 display模块索引-显示窗口](https://blog.csdn.net/qq_48979387/article/details/129219749#t3)

[14 声音](https://blog.csdn.net/qq_48979387/article/details/129219749#t4)

[14.1 设定混音器](https://blog.csdn.net/qq_48979387/article/details/129219749#t5)

[14.2 播放音效](https://blog.csdn.net/qq_48979387/article/details/129219749#t6)

[14.3 播放背景音乐](https://blog.csdn.net/qq_48979387/article/details/129219749#t7)

[14.4 mixer模块索引-混音器](https://blog.csdn.net/qq_48979387/article/details/129219749#t8)

[14.4 mixer.music模块索引-背景音乐](https://blog.csdn.net/qq_48979387/article/details/129219749#t9)

[15 坐标处理](https://blog.csdn.net/qq_48979387/article/details/129219749#t10)

[15.1 矩形](https://blog.csdn.net/qq_48979387/article/details/129219749#t11)

[15.2 Rect对象方法索引](https://blog.csdn.net/qq_48979387/article/details/129219749#t12)

[15.3 FRect - 支持浮点运算的矩形](https://blog.csdn.net/qq_48979387/article/details/129219749#t13)

[15.4 向量](https://blog.csdn.net/qq_48979387/article/details/129219749#t14)

[15.5 Vector2对象方法索引-2D向量](https://blog.csdn.net/qq_48979387/article/details/129219749#t15)

[15.6 Vector3对象方法索引-3D向量](https://blog.csdn.net/qq_48979387/article/details/129219749#t16)

[15.7 其他几何图形对象](https://blog.csdn.net/qq_48979387/article/details/129219749#t17)

[16 数学库](https://blog.csdn.net/qq_48979387/article/details/129219749#t18)

[16.1 限制数值范围](https://blog.csdn.net/qq_48979387/article/details/129219749#t19)

[16.2 插值](https://blog.csdn.net/qq_48979387/article/details/129219749#t20)

## (6)

link: <https://blog.csdn.net/qq_48979387/article/details/129476079>

[17 游戏手柄](https://blog.csdn.net/qq_48979387/article/details/129476079#t0)

[17.1 游戏手柄结构](https://blog.csdn.net/qq_48979387/article/details/129476079#t1)

[17.2 载入游戏手柄](https://blog.csdn.net/qq_48979387/article/details/129476079#t2)

[17.3 游戏手柄事件](https://blog.csdn.net/qq_48979387/article/details/129476079#t3)

[17.4 使用游戏手柄的示例](https://blog.csdn.net/qq_48979387/article/details/129476079#t4)

[17.5 joystick模块索引-游戏手柄](https://blog.csdn.net/qq_48979387/article/details/129476079#t5)

[18 摄像头](https://blog.csdn.net/qq_48979387/article/details/129476079#t6)

[18.1 从摄像头获取图片](https://blog.csdn.net/qq_48979387/article/details/129476079#t7)

[18.2 camera模块索引-摄像头](https://blog.csdn.net/qq_48979387/article/details/129476079#t8)

[19 系统](https://blog.csdn.net/qq_48979387/article/details/129476079#t9)

[19.1 存储游戏数据](https://blog.csdn.net/qq_48979387/article/details/129476079#t10)

[19.2 获取系统区域设置](https://blog.csdn.net/qq_48979387/article/details/129476079#t11)

[19.3 system模块索引-系统](https://blog.csdn.net/qq_48979387/article/details/129476079#t12)

[20 剪贴板](https://blog.csdn.net/qq_48979387/article/details/129476079#t13)

[20.1 获取剪贴板中的文本](https://blog.csdn.net/qq_48979387/article/details/129476079#t14)

[20.2 复制文本到剪贴板](https://blog.csdn.net/qq_48979387/article/details/129476079#t15)

## (7)

link: <https://blog.csdn.net/qq_48979387/article/details/130784914>

[21 OpenGL与Pygame](https://blog.csdn.net/qq_48979387/article/details/130784914#t0)

[21.1 OpenGL简介](https://blog.csdn.net/qq_48979387/article/details/130784914#t1)

[21.2 支持OpenGL的窗口](https://blog.csdn.net/qq_48979387/article/details/130784914#t2)

[22 表面像素与内存](https://blog.csdn.net/qq_48979387/article/details/130784914#t3)

[22.1 设置与获取表面单个像素](https://blog.csdn.net/qq_48979387/article/details/130784914#t4)

[22.2 锁定表面内存](https://blog.csdn.net/qq_48979387/article/details/130784914#t5)

[22.3 像素数组](https://blog.csdn.net/qq_48979387/article/details/130784914#t6)

[22.4 PixelArray对象方法索引](https://blog.csdn.net/qq_48979387/article/details/130784914#t7)

[22.5 surfarray模块索引 - Surface与numpy数组](https://blog.csdn.net/qq_48979387/article/details/130784914#t8)

[22.6 BufferProxy对象方法索引](https://blog.csdn.net/qq_48979387/article/details/130784914#t9)

## (8)

link: <https://blog.csdn.net/qq_48979387/article/details/131957938>

[23 进阶声音操作](https://blog.csdn.net/qq_48979387/article/details/131957938#t0)

[23.1 通过MIDI输出声音](https://blog.csdn.net/qq_48979387/article/details/131957938#t1)

[23.2 模拟乐器音色](https://blog.csdn.net/qq_48979387/article/details/131957938#t2)

[23.3 写入原始MIDI信息数据](https://blog.csdn.net/qq_48979387/article/details/131957938#t3)

[23.4 midi模块索引 - 乐器数字接口](https://blog.csdn.net/qq_48979387/article/details/131957938#t4)

[23.5 sndarray模块索引 - Sound与numpy数组](https://blog.csdn.net/qq_48979387/article/details/131957938#t5)

[24 pygame探索](https://blog.csdn.net/qq_48979387/article/details/131957938#t6)

[24.1 pygame主模块索引](https://blog.csdn.net/qq_48979387/article/details/131957938#t7)

[24.2 环境变量](https://blog.csdn.net/qq_48979387/article/details/131957938#t8)

[24.3 _sdl2](https://blog.csdn.net/qq_48979387/article/details/131957938#t9)

[25 性能提升](https://blog.csdn.net/qq_48979387/article/details/131957938#t10)

[25.1 GPU](https://blog.csdn.net/qq_48979387/article/details/131957938#t11)

[25.2 SCALED标志](https://blog.csdn.net/qq_48979387/article/details/131957938#t12)

[25.3 _sdl2.video](https://blog.csdn.net/qq_48979387/article/details/131957938#t13)

[26 打包pygame](https://blog.csdn.net/qq_48979387/article/details/131957938#t14)

[26.1 用pyinstaller打包pygame](https://blog.csdn.net/qq_48979387/article/details/131957938#t15)

[26.2 在网页上运行pygame](https://blog.csdn.net/qq_48979387/article/details/131957938#t16)

[26.3 在Andriod上运行pygame](https://blog.csdn.net/qq_48979387/article/details/131957938#t17)

[26.4 在IOS上运行pygame（探索中）](https://blog.csdn.net/qq_48979387/article/details/131957938#t18)

[27 关于pygame使用的一些总结](https://blog.csdn.net/qq_48979387/article/details/131957938#t19)

[27.1 FPS相关注意点](https://blog.csdn.net/qq_48979387/article/details/131957938#t20)

[FPS的设定](https://blog.csdn.net/qq_48979387/article/details/131957938#t21)

[实际FPS不稳定](https://blog.csdn.net/qq_48979387/article/details/131957938#t22)

[27.2 物理引擎](https://blog.csdn.net/qq_48979387/article/details/131957938#t23)

[附录 easy_pygame](https://blog.csdn.net/qq_48979387/article/details/131957938#t24)