# Python pymunk最完整教程 总目录

专栏地址: <https://blog.csdn.net/qq_48979387/category_12729101.html>

## (1)

link: <https://blog.csdn.net/qq_48979387/article/details/140271488>

[1 简介](https://blog.csdn.net/qq_48979387/article/details/140271488#t0)

[1.1 物理引擎](https://blog.csdn.net/qq_48979387/article/details/140271488#t1)

[1.2 pymunk](https://blog.csdn.net/qq_48979387/article/details/140271488#t2)

[1.3 关于本教程](https://blog.csdn.net/qq_48979387/article/details/140271488#t3)

[2 基础功能](https://blog.csdn.net/qq_48979387/article/details/140271488#t4)

[2.1 第一个示例](https://blog.csdn.net/qq_48979387/article/details/140271488#t5)

[2.2 空间](https://blog.csdn.net/qq_48979387/article/details/140271488#t6)

[2.3 身体和形状](https://blog.csdn.net/qq_48979387/article/details/140271488#t7)

[2.4 物理属性](https://blog.csdn.net/qq_48979387/article/details/140271488#t8)

[2.5 设置重力和阻尼](https://blog.csdn.net/qq_48979387/article/details/140271488#t9)

[3 身体](https://blog.csdn.net/qq_48979387/article/details/140271488#t10)

[3.1 对物体施加力和冲量](https://blog.csdn.net/qq_48979387/article/details/140271488#t11)

[3.2 身体类型](https://blog.csdn.net/qq_48979387/article/details/140271488#t12)

[3.3 睡眠](https://blog.csdn.net/qq_48979387/article/details/140271488#t13)

[3.4 Body常用实例属性](https://blog.csdn.net/qq_48979387/article/details/140271488#t14)

[3.5 通过回调函数管理身体行为](https://blog.csdn.net/qq_48979387/article/details/140271488#t15)

[3.6 Body对象属性参考](https://blog.csdn.net/qq_48979387/article/details/140271488#t16)

[4 数学运算辅助](https://blog.csdn.net/qq_48979387/article/details/140271488#t17)

[4.1 向量](https://blog.csdn.net/qq_48979387/article/details/140271488#t18)

[4.2 Vec2d对象属性参考](https://blog.csdn.net/qq_48979387/article/details/140271488#t19)

[4.3 矩形边界框](https://blog.csdn.net/qq_48979387/article/details/140271488#t20)

[4.4 BB对象属性参考](https://blog.csdn.net/qq_48979387/article/details/140271488#t21)

## (2)

link: <https://blog.csdn.net/qq_48979387/article/details/140383410>

[5 形状](https://blog.csdn.net/qq_48979387/article/details/140383410#t0)

[5.1 基本形状](https://blog.csdn.net/qq_48979387/article/details/140383410#t1)

[5.2 摩擦系数](https://blog.csdn.net/qq_48979387/article/details/140383410#t2)

[5.3 弹性系数](https://blog.csdn.net/qq_48979387/article/details/140383410#t3)

[5.4 形状变换](https://blog.csdn.net/qq_48979387/article/details/140383410#t4)

[5.5 Shape对象属性参考](https://blog.csdn.net/qq_48979387/article/details/140383410#t5)

[5.6 Transform对象属性参考](https://blog.csdn.net/qq_48979387/article/details/140383410#t6)

[6 约束](https://blog.csdn.net/qq_48979387/article/details/140383410#t7)

[6.1 创建PinJoint约束](https://blog.csdn.net/qq_48979387/article/details/140383410#t8)

[6.2 更多约束类](https://blog.csdn.net/qq_48979387/article/details/140383410#t9)

[6.3 Constraint对象属性参考](https://blog.csdn.net/qq_48979387/article/details/140383410#t10)

[7 碰撞检测](https://blog.csdn.net/qq_48979387/article/details/140383410#t11)

[7.1 碰撞处理器](https://blog.csdn.net/qq_48979387/article/details/140383410#t12)

[7.2 仲裁器](https://blog.csdn.net/qq_48979387/article/details/140383410#t13)

[7.3 CollisionHandler对象属性参考](https://blog.csdn.net/qq_48979387/article/details/140383410#t14)

[7.4 Arbiter对象属性参考](https://blog.csdn.net/qq_48979387/article/details/140383410#t15)

[7.5 添加碰撞处理器的其他方法](https://blog.csdn.net/qq_48979387/article/details/140383410#t16)

[7.6 形状查询](https://blog.csdn.net/qq_48979387/article/details/140383410#t17)

[7.7 在空间中查询](https://blog.csdn.net/qq_48979387/article/details/140383410#t18)

[7.8 形状过滤器](https://blog.csdn.net/qq_48979387/article/details/140383410#t19)

[7.9 传感器](https://blog.csdn.net/qq_48979387/article/details/140383410#t20)

## (3)

link: <https://blog.csdn.net/qq_48979387/article/details/140590359>

[8 空间管理](https://blog.csdn.net/qq_48979387/article/details/140590359#t0)

[8.1 管理身体、形状、约束](https://blog.csdn.net/qq_48979387/article/details/140590359#t1)

[8.2 static_body](https://blog.csdn.net/qq_48979387/article/details/140590359#t2)

[8.3 复制和转储空间](https://blog.csdn.net/qq_48979387/article/details/140590359#t3)

[8.4 Space对象属性参考](https://blog.csdn.net/qq_48979387/article/details/140590359#t4)

[9 其他功能](https://blog.csdn.net/qq_48979387/article/details/140590359#t5)

[9.1 自动几何图形转换](https://blog.csdn.net/qq_48979387/article/details/140590359#t6)

[9.2 调试](https://blog.csdn.net/qq_48979387/article/details/140590359#t7)

[9.3 其他子模块](https://blog.csdn.net/qq_48979387/article/details/140590359#t8)

[batch](https://blog.csdn.net/qq_48979387/article/details/140590359#t9)

[examples](https://blog.csdn.net/qq_48979387/article/details/140590359#t10)

[10 总结](https://blog.csdn.net/qq_48979387/article/details/140590359#t11)