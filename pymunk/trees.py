trees = [["https://blog.csdn.net/qq_48979387/article/details/140271488","""1 简介
1.1 物理引擎
1.2 pymunk
1.3 关于本教程
2 基础功能
2.1 第一个示例
2.2 空间
2.3 身体和形状
2.4 物理属性
2.5 设置重力和阻尼
3 身体
3.1 对物体施加力和冲量
3.2 身体类型
3.3 睡眠
3.4 Body常用实例属性
3.5 通过回调函数管理身体行为
3.6 Body对象属性参考
4 数学运算辅助
4.1 向量
4.2 Vec2d对象属性参考
4.3 矩形边界框
4.4 BB对象属性参考"""],
["https://blog.csdn.net/qq_48979387/article/details/140383410","""5 形状
5.1 基本形状
5.2 摩擦系数
5.3 弹性系数
5.4 形状变换
5.5 Shape对象属性参考
5.6 Transform对象属性参考
6 约束
6.1 创建PinJoint约束
6.2 更多约束类
6.3 Constraint对象属性参考
7 碰撞检测
7.1 碰撞处理器
7.2 仲裁器
7.3 CollisionHandler对象属性参考
7.4 Arbiter对象属性参考
7.5 添加碰撞处理器的其他方法
7.6 形状查询
7.7 在空间中查询
7.8 形状过滤器
7.9 传感器"""],
["https://blog.csdn.net/qq_48979387/article/details/140590359","""8 空间管理
8.1 管理身体、形状、约束
8.2 static_body
8.3 复制和转储空间
8.4 Space对象属性参考
9 其他功能
9.1 自动几何图形转换
9.2 调试
9.3 其他子模块
batch
examples
10 总结"""]]
